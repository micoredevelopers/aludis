<?php
    $this_category = get_category($cat);
	//If category is parent, list it
    if ($this_category->category_parent == 0) {
		$this_category->category_parent = $cat;
    } else { // If category is not parent, list parent category
		$parent_category = get_category($this_category->category_parent);
		//get the parent category id
		$ParentCatId = $parent_category->cat_ID;
		}
		$childcategories = array();
		$catx = $ParentCatId;
		$cats = get_categories('parent='.$catx);
		foreach($cats as $cat) {
			$childcategories[] = $cat->term_id; } ;
		//if it is a child category, add a class with the parent category id
		if( is_category( $childcategories ) ) {
			$class = 'parent-category-'.$ParentCatId;
		}
?> 

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
		<meta http-equiv="X-UA-Compatible" content="ie=edge, chrome=1" />
        <?php wp_head(); ?>
		<title>Aludis</title>
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri() ?>/favicon.ico" />

        <link href="<?php echo get_stylesheet_directory_uri() ?>/styles/main.css" rel="stylesheet" />
        <link href="<?php echo get_stylesheet_directory_uri() ?>/styles/gruby_edit.css" rel="stylesheet" />
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NPM99JX');</script>
		<!-- End Google Tag Manager -->
		<meta name="google-site-verification" content="WPfHKPer2YJYB61g9oUyY8-9LdTyd8RKVLGCKxfdIXI" />
		<meta name="yandex-verification" content="dacead84773b81bf" />
		<script>
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
  }
</script>


<script type="application/ld+json">
      {
        "@context": "https://schema.org/",
        "@type": "BreadcrumbList",
        "itemListElement": [
          {
            "@type": "ListItem",
            "position": 1,
            "name": "vorota",
            "item": "http://aludis.comnd-x.com/category/vorota/"
          },
          {
            "@type": "ListItem",
            "position": 2,
            "name": "rollety",
            "item": "http://aludis.comnd-x.com/category/rollety/"
          },
          {
            "@type": "ListItem",
            "position": 3,
            "name": "avtomatika",
            "item": "http://aludis.comnd-x.com/category/avtomatika/"
          },
          {
            "@type": "ListItem",
            "position": 4,
            "name": "kontakty",
            "item": "http://aludis.comnd-x.com/kontakty/"
          }
        ]
      }
    </script>
    <script type="application/ld+json">
      {
        "@context": "https://schema.org",
        "@type": "Organization",
        "url": "http://aludis.comnd-x.com",
        "logo": "http://aludis.comnd-x.com/wp-content/themes/aludis_theme/assets/images/logo.svg"
      }
    </script>
    <script type="application/ld+json">
      {
        "@context": "https://schema.org",
        "@type": "Organisation",
        "image": ["https://example.com/photos/1x1/photo.jpg", "https://example.com/photos/4x3/photo.jpg", "https://example.com/photos/16x9/photo.jpg"],
        "@id": "http://aludis.comnd-x.com",
        "name": "Aludis",
        "address": {
          "@type": "PostalAddress",
          "streetAddress": "148 W 51st St",
          "addressLocality": "New York",
          "addressRegion": "NY",
          "postalCode": "10019",
          "addressCountry": "US"
        },
        "geo": {
          "@type": "GeoCoordinates",
          "latitude": 40.761293,
          "longitude": -73.982294
        },
        "url": "http://www.example.com/restaurant-locations/manhattan",
        "telephone": "+12122459600",
        "servesCuisine": "American",
        "priceRange": "$$$",
        "openingHoursSpecification": [
          {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": ["Monday", "Tuesday"],
            "opens": "11:30",
            "closes": "22:00"
          },
          {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": ["Wednesday", "Thursday", "Friday"],
            "opens": "11:30",
            "closes": "23:00"
          },
          {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": "Saturday",
            "opens": "16:00",
            "closes": "23:00"
          },
          {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": "Sunday",
            "opens": "16:00",
            "closes": "22:00"
          }
        ]
      }
    </script>



    </head>
	<body <?php body_class($class); ?>>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NPM99JX"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<header class="header">
			<div class="container">
				<div class="header__row">
					<div class="header__actions">
						<div class="header__menu d-flex d-lg-none" data-aos="show-up-20" data-aos-delay="200">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</div>
						<a class="header__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" data-aos="show-up-20" data-aos-delay="400"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo.svg" alt="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" /></a>
						<ul class="header__links-list d-none d-lg-flex" data-aos="show-up-20" data-aos-delay="600">
                        <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'items_wrap' => '%3$s', 'container' => '', 'link_before' => '<span>', 'link_after' => '</span>', 'walker' => new My_Walker_Nav_Menu() )
                            ); ?>
							
						</ul>
						<a class="header__call d-flex d-lg-none" href="tel: <?php the_field('phone_field', 'option'); ?>" data-aos="show-up-20" data-aos-delay="600">
							<img class="svg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/call_mobile.svg" alt="Feedback" />
						</a>
					</div>
					<div class="header__actions d-none d-lg-flex">
						<a class="header__call" href="mailto:<?php the_field('email_field', 'option'); ?>" data-aos="show-up-20" data-aos-delay="800">
							<img class="svg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/mail.svg" alt="Mail" />
							<span><?php the_field('email_field', 'option'); ?></span>
						</a>
						<a class="header__call" href="tel: <?php the_field('phone_field', 'option'); ?>" data-aos="show-up-20" data-aos-delay="1000">
							<img class="svg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/call.svg" alt="Mail" />
							<span><?php the_field('phone_field', 'option'); ?></span>
						</a>
					</div>
				</div>
			</div>
		</header>