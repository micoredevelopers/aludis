<?php
add_action( 'after_setup_theme', 'aludis_setup' );
function aludis_setup() {
load_theme_textdomain( 'aludis', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );

add_theme_support( 'post-thumbnails' );
add_image_size( 'slider', 1280, 860, true);
add_image_size( 'homepage', 280, 200, true);
add_image_size( 'avatar', 32, 32, true);

add_theme_support( 'html5', array( 'search-form' ) );
global $content_width;
if ( ! isset( $content_width ) ) { $content_width = 1920; }
register_nav_menus( array( 'main-menu' => esc_html__( 'Main Menu', 'aludis' ) ) );
register_nav_menus( array( 'mob-menu' => esc_html__( 'Mob Menu', 'aludis' ) ) );
register_nav_menus( array( 'footer-menu' => esc_html__( 'Footer Menu', 'aludis' ) ) );
register_nav_menus( array( 'rr-menu' => esc_html__( 'Подменю роллетов', 'aludis' ) ) );
register_nav_menus( array( 'vv-menu' => esc_html__( 'Подменю ворот', 'aludis' ) ) );
register_nav_menus( array( 'aa-menu' => esc_html__( 'Подменю автоматики', 'aludis' ) ) );
}
add_action( 'wp_enqueue_scripts', 'aludis_load_scripts' );
function aludis_load_scripts() {
wp_enqueue_style( 'aludis-style', get_stylesheet_uri() );
wp_enqueue_script( 'jquery' );
}
add_action( 'wp_footer', 'aludis_footer_scripts' );
function aludis_footer_scripts() {
?>
<script>
jQuery(document).ready(function ($) {
var deviceAgent = navigator.userAgent.toLowerCase();
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
$("html").addClass("ios");
$("html").addClass("mobile");
}
if (navigator.userAgent.search("MSIE") >= 0) {
$("html").addClass("ie");
}
else if (navigator.userAgent.search("Chrome") >= 0) {
$("html").addClass("chrome");
}
else if (navigator.userAgent.search("Firefox") >= 0) {
$("html").addClass("firefox");
}
else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
$("html").addClass("safari");
}
else if (navigator.userAgent.search("Opera") >= 0) {
$("html").addClass("opera");
}
});
</script>
<?php
}
add_filter( 'document_title_separator', 'aludis_document_title_separator' );
function aludis_document_title_separator( $sep ) {
$sep = '|';
return $sep;
}
add_filter( 'the_title', 'aludis_title' );
function aludis_title( $title ) {
if ( $title == '' ) {
return '...';
} else {
return $title;
}
}
add_filter( 'the_content_more_link', 'aludis_read_more_link' );
function aludis_read_more_link() {
if ( ! is_admin() ) {
return ' <a href="' . esc_url( get_permalink() ) . '" class="more-link">...</a>';
}
}
add_filter( 'excerpt_more', 'aludis_excerpt_read_more_link' );
function aludis_excerpt_read_more_link( $more ) {
if ( ! is_admin() ) {
global $post;
return ' <a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="more-link">...</a>';
}
}
add_filter( 'intermediate_image_sizes_advanced', 'aludis_image_insert_override' );
function aludis_image_insert_override( $sizes ) {
unset( $sizes['medium_large'] );
return $sizes;
}
add_action( 'widgets_init', 'aludis_widgets_init' );
function aludis_widgets_init() {
register_sidebar( array(
'name' => esc_html__( 'Sidebar Widget Area', 'aludis' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => '</li>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
add_action( 'wp_head', 'aludis_pingback_header' );
function aludis_pingback_header() {
if ( is_singular() && pings_open() ) {
printf( '<link rel="pingback" href="%s" />' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
}
}
add_action( 'comment_form_before', 'aludis_enqueue_comment_reply_script' );
function aludis_enqueue_comment_reply_script() {
if ( get_option( 'thread_comments' ) ) {
wp_enqueue_script( 'comment-reply' );
}
}
function aludis_custom_pings( $comment ) {
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'aludis_comment_count', 0 );
function aludis_comment_count( $count ) {
if ( ! is_admin() ) {
global $id;
$get_comments = get_comments( 'status=approve&post_id=' . $id );
$comments_by_type = separate_comments( $get_comments );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}


// function add_menuclass($ulclass) {
//     return preg_replace('/<a /', '<a class="header__link"', $ulclass);
//  }
//  add_filter('wp_nav_menu','add_menuclass');

 function be_arrows_in_menus( $item_output, $item, $depth, $args ) {

	if( in_array( 'menu-item-has-children', $item->classes ) ) {
		$arrow = 0 == $depth ? '<img class="svg" src="' . get_stylesheet_directory_uri() . '/assets/images/link_arrow.svg" />' : '<img class="svg" src="' . get_stylesheet_directory_uri() . '/assets/images/link_arrow.svg" />';
		$item_output = str_replace( '</a>', '</a>' . $arrow, $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'be_arrows_in_menus', 10, 4 );



class My_Walker_Nav_Menu extends Walker_Nav_Menu {

    function start_lvl(&$output, $depth){

    $indent = str_repeat("\t", $depth);

    $output .= "\n$indent<ul class=\"header__sub-category-list\">\n"; 

      }}


class Mob_Walker_Nav_Menu extends Walker_Nav_Menu {

    function start_lvl(&$output, $depth){

    $indent = str_repeat("\t", $depth);

    $output .= "\n$indent<div class=\"global-menu__sub-wrap\"><ul class=\"global-menu__sub-list\">\n"; 

      }}


      class Description_Walker extends Walker_Nav_Menu {
        function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
          $classes = empty($item->classes) ? array () : (array) $item->classes;
          $class_names = join(' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
          !empty ( $class_names ) and $class_names = ' class="footer__link '. esc_attr( $class_names ) . '"';
          $output .= "";
          $attributes  = '';
          !empty( $item->attr_title ) and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
          !empty( $item->target ) and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
          !empty( $item->xfn ) and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
          !empty( $item->url ) and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';
          $title = apply_filters( 'the_title', $item->title, $item->ID );
          $item_output = $args->before
          . "<a $attributes $class_names>"
          . $args->link_before
          . $title
          . '</a>'
          . $args->link_after
          . $args->after;
          $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        }
      }





      class Cat_Description_Walker extends Walker_Nav_Menu {
        function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
          $classes = empty($item->classes) ? array () : (array) $item->classes;
          $class_names = join(' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
          !empty ( $class_names ) and $class_names = ' class="sub-category-link '. esc_attr( $class_names ) . '"';
          $output .= "";
          $attributes  = '';
          !empty( $item->attr_title ) and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
          !empty( $item->target ) and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
          !empty( $item->xfn ) and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
          !empty( $item->url ) and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';
          $title = apply_filters( 'the_title', $item->title, $item->ID );
          $item_output = $args->before
          . "<a $attributes $class_names>"
          . $args->link_before
          . $title
          . '</a>'
          . $args->link_after
          . $args->after;
          $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        }
      }
      
      remove_filter( 'the_content', 'wpautop' );
      
      function dmcg_allow_nbsp_in_tinymce( $init ) {
        $init['entities'] = '160,nbsp,38,amp,60,lt,62,gt';   
        $init['entity_encoding'] = 'named';
        return $init;
    }
    add_filter( 'tiny_mce_before_init', 'dmcg_allow_nbsp_in_tinymce' );

      function my_custom_post_slider() {
        $labels = array(
          'name'               => _x( 'Слайдер', 'post type general name' ),
          'singular_name'      => _x( 'Слайд', 'post type singular name' ),
          'add_new'            => _x( 'Добавить слайд', 'book' ),
          'add_new_item'       => __( 'Добавить новый слайд' ),
          'edit_item'          => __( 'Редактировать слайд' ),
          'new_item'           => __( 'Новый слайд' ),
          'all_items'          => __( 'Все слайды' ),
          'view_item'          => __( 'Показать слайды' ),
          'search_items'       => __( 'Найти слайды' ),
          'not_found'          => __( 'Сладов не найдено' ),
          'not_found_in_trash' => __( 'В корзине слайдов не найдено' ), 
          'parent_item_colon'  => ’,
          'menu_name'          => 'Слайдер'
        );
        $args = array(
          'labels'        => $labels,
          'description'   => 'Holds our products and product specific data',
          'public'        => true,
          'menu_position' => 5,
          'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
          'has_archive'   => true,        
          'menu_icon'     => 'dashicons-images-alt2',
        );
        register_post_type( 'slider', $args ); 
      }
      add_action( 'init', 'my_custom_post_slider' );      
      
      
      function my_custom_post_reviews() {
        $labels = array(
          'name'               => _x( 'Отзывы', 'post type general name' ),
          'singular_name'      => _x( 'Отзыв', 'post type singular name' ),
          'add_new'            => _x( 'Добавить отзыв', 'book' ),
          'add_new_item'       => __( 'Добавить новый отзыв' ),
          'edit_item'          => __( 'Редактировать отзыв' ),
          'new_item'           => __( 'Новый сотзывлайд' ),
          'all_items'          => __( 'Все отзывы' ),
          'view_item'          => __( 'Показать отзывы' ),
          'search_items'       => __( 'Найти отзывы' ),
          'not_found'          => __( 'Отзывов не найдено' ),
          'not_found_in_trash' => __( 'В корзине отзывов не найдено' ), 
          'parent_item_colon'  => ’,
          'menu_name'          => 'Отзывы'
        );
        $args = array(
          'labels'        => $labels,
          'description'   => 'Holds our products and product specific data',
          'public'        => true,
          'menu_position' => 5,
          'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
          'has_archive'   => true,        
          'menu_icon'     => 'dashicons-format-chat',
        );
        register_post_type( 'reviews', $args ); 
      }
      add_action( 'init', 'my_custom_post_reviews' );



      add_action( 'init', 'cpt_mail_calback' );

      function cpt_mail_calback() {
      
          $labels = array(
              "name" => "Заявки",
              "singular_name" => "Заявка",
              "menu_name" => "Заявки",
              "all_items" => "Все заявки",
              "add_new" => "Добавить заявку",
              "add_new_item" => "Добавить новую заявку",
              "edit" => "Редактировать заявку",
              "edit_item" => "Редактировать заявку",
              "new_item" => "Новая заявка",
              "view" => "Показать заявку",
              "view_item" => "Показать заявку",
              "search_items" => "Найти заявку",
              "not_found" => "Заявок не найдено",
              "not_found_in_trash" => "В корзине заявок не найдено",
          );
      
          $args = array(
              "labels" => $labels,
              "description" => "",
              "public" => true,
              "show_ui" => true,
              "has_archive" => false,
              "show_in_menu" => true,
              "exclude_from_search" => true,
              "capability_type" => "post",
              "map_meta_cap" => true,
              "hierarchical" => true,
              "rewrite" => false,
              "query_var" => true,
              "menu_position" => 7,
              "menu_icon" => "dashicons-email-alt",
              "supports" => array( "title", "editor" ),
          );
      
          register_post_type( "mail", $args );
      
      }


      function send_mail() {

        /* Забираем отправленные данные */
        $client_fio = $_POST['name'];
        $client_phone = $_POST['phone'];
        $client_quest = $_POST['question'];
      
        /* Отправляем нам письмо */
        $emailTo = 'eugenegruby@gmail.com';
        $subject = 'Test mail рассылки!';
        $headers = "Content-type: text/html; charset=\"utf-8\"";
        $mailBody = "$client_fio <br/><br/> $client_phone <br/><br/> $client_quest";
      
        wp_mail($emailTo, $subject, $mailBody, $headers);
      
        /* Создаем новый пост-письмо */
        $post_data = array(
         'post_title'    => $client_fio . ' - ' .$client_phone,
         'post_content'  => $client_quest,
         'post_status'   => 'publish',
         'post_author'   => 1,
         'post_type' => 'mail',
        );
      
        wp_insert_post( $post_data );
      
        /* Завершаем выполнение ajax */
        die();
        
      }
      
      add_action("wp_ajax_send_mail", "send_mail");
      add_action("wp_ajax_nopriv_send_mail", "send_mail");





      function send_review() {

        /* Забираем отправленные данные */
        $reviewer_name = $_POST['name'];
        $reviewer_message = $_POST['review'];
      
        /* Отправляем нам письмо */
        $emailTo = 'eugenegruby@gmail.com';
        $subject = "Новый отзыв на сайте от $reviewer_name";
        $headers = "Content-type: text/html; charset=\"utf-8\"";
        $mailBody = "$reviewer_name <br/><br/> $reviewer_message";
      
        wp_mail($emailTo, $subject, $mailBody, $headers);
      
        /* Создаем новый пост-письмо */
        $post_data = array(
         'post_title'    => $reviewer_name,
         'post_content'  => $reviewer_message,
         'post_status'   => 'pending',
         'post_author'   => 1,
         'post_type' => 'reviews',
        );
      
        wp_insert_post( $post_data );
      
        /* Завершаем выполнение ajax */
        die();
        
      }
      
      add_action("wp_ajax_send_review", "send_review");
      add_action("wp_ajax_nopriv_send_review", "send_review");


      remove_filter('term_description','wpautop');


      // make category use parent category template
      function load_cat_parent_template($template) {

        $cat_ID = absint( get_query_var('cat') );
        $category = get_category( $cat_ID );
      
        $templates = array();
      
        if ( !is_wp_error($category) )
            $templates[] = "category-{$category->slug}.php";
      
        $templates[] = "category-$cat_ID.php";
      
        // trace back the parent hierarchy and locate a template
        if ( !is_wp_error($category) ) {
            $category = $category->parent ? get_category($category->parent) : '';
      
            if( !empty($category) ) {
                if ( !is_wp_error($category) )
                    $templates[] = "category-{$category->slug}.php";
      
                $templates[] = "category-{$category->term_id}.php";
            }
        }
      
        $templates[] = "category.php";
        $template = locate_template($templates);
      
        return $template;
      }
      add_action('category_template', 'load_cat_parent_template');