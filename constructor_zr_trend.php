<?php /* Template Name: Конструктор — Защитные роллеты — Trend */ ?>
  


<!DOCTYPE html>
  <html <?php language_attributes(); ?>>
	  <head>
		  <meta charset="<?php bloginfo( 'charset' ); ?>" />
		  <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
		  <meta http-equiv="X-UA-Compatible" content="ie=edge, chrome=1" />
		  <?php wp_head(); ?>
		  <title>Aludis</title>
		  <link rel="icon" href="<?php echo get_stylesheet_directory_uri() ?>/favicon.ico" />
  
		  <link href="<?php echo get_stylesheet_directory_uri() ?>/styles/main.css" rel="stylesheet" />
		  <link href="<?php echo get_stylesheet_directory_uri() ?>/styles/gruby_edit.css" rel="stylesheet" />
		  <!-- Google Tag Manager -->
		  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		  })(window,document,'script','dataLayer','GTM-NPM99JX');</script>
		  <!-- End Google Tag Manager -->
		  <meta name="google-site-verification" content="WPfHKPer2YJYB61g9oUyY8-9LdTyd8RKVLGCKxfdIXI" />
		  <meta name="yandex-verification" content="dacead84773b81bf" />
	  </head>
	  <body style="background-color: transparent;">
  
  
  
  
  <main id="sub-category-page"> 
  
  
  <?php
		  $count = 0;
		  $your_repeater = get_field('зр-блок_преимуществ', 'option');
		  if($your_repeater){
		  while( have_rows('зр-блок_преимуществ', 'option') ): the_row();
		  $count++;
		  if ($count == 3) { 
	  ?>
  
			  <section class="sub-category-appearance-section sba-<?php echo $count;?>" data-aos="show-up-20" style="padding: 0px 0;">
					  <h2 class="section-title" style="padding-left: 30px;">Внешний вид</h2>
					  <div class="row">
						  <div class="col-12 col-lg-6 col-appearance">
						  <?php while( have_rows('zr-konstruktor', 'option') ): the_row(); ?>
  
  
  
  
						  <?php endwhile; ?>
						  <?php if( have_rows('zr-vkladki_konstruktora', 'option') ): $i = 0; ?>
								  <?php while( have_rows('zr-vkladki_konstruktora', 'option') ): the_row(); $i++; ?>
								  <?php if( $i ==1 ){ ?><div class="choose-image-wrap" style="background-position: center center; background-repeat: no-repeat; background-size: cover;"></div><?php } ?>
							  <?php endwhile; ?>
							  <?php endif; ?>
						  </div>
						  <div class="col-12 col-lg-6">
							  <div class="choose-type-wrap">
								  <p class="choose-title">Типы профиля</p>
								  <ul class="nav nav-tab">
								  <?php if( have_rows('zr-vkladki_konstruktora', 'option') ): $i = 0; ?>
								  <?php while( have_rows('zr-vkladki_konstruktora', 'option') ): the_row(); $i++; ?>
									  <li class="nav-item">
										  <a class="choose-item type-item <?php if( $i ==1 ){ echo "active"; } ?>" data-toggle="tab" href="#tab-<?php the_sub_field('zr-konstruktor_nomer_vkladki'); ?>" id="the-tab-<?php the_sub_field('zr-konstruktor_nomer_vkladki'); ?>">
											  <div class="choose-item__image" data-img-link="<?php the_sub_field('zr-konstruktor_polnaya_kartinka_vkladki'); ?>"><img src="<?php the_sub_field('zr-konstruktor_kartinka_vkladki'); ?>" class="profile-type" alt="type" /></div>
											  <span class="choose-item__text"><?php the_sub_field('zr-konstruktor_imya_vkladki'); ?></span>
										  </a>
									  </li>
								  <?php endwhile; ?>
								  <?php endif; ?>
  
								  </ul>
							  </div>
							  <div class="choose-type-wrap">
								  <p class="choose-title">Палитра</p>
								  <div class="tab-content">
								  <?php if( have_rows('zr-konstruktor', 'option') ): $i = 0; ?>
								  <?php while( have_rows('zr-konstruktor', 'option') ): the_row(); $i++; ?>
									  <div class="tab-pane fade show <?php if( $i ==1 ){ echo "active"; } ?>" id="tab-<?php the_sub_field('zr-konstruktor_vkladka'); ?>">
										  <p class="choose-sub-title">Стандартная</p>
										  <ul class="choose-items">
										  <?php if( have_rows('zr-czveta_i_kartinki', 'option') ): $i = 0; ?>
										  <?php while( have_rows('zr-czveta_i_kartinki', 'option') ): the_row(); $i++; ?>
											  <div class="choose-item palette-item <?php if( $i ==1 ){ echo "active"; } ?>" data-type="color">
												  <div class="choose-item__color" data-color="<?php the_sub_field('zr-konstruktor_izobrazhenie'); ?>" style="background: url(<?php the_sub_field('zr-konstruktor_izobrazhenie'); ?>) no-repeat center center/cover;"></div>
												  <p class="choose-item__text"><?php the_sub_field('zr-konstruktor_imya_czveta'); ?></p>
											  </div>
										  <?php endwhile; ?>
										  <?php endif; ?>
										  </ul>
										  <?php if( have_rows('zr-czveta_i_kartinki_derevo', 'option') ): $i = 0; ?>
										  <p class="choose-sub-title">Под дерево</p>
										  <ul class="choose-items">
										  <?php while( have_rows('zr-czveta_i_kartinki_derevo', 'option') ): the_row(); $i++; ?>
											  <div class="choose-item palette-item <?php if( $i ==1 ){ echo "active"; } ?>" data-type="color">
												  <div class="choose-item__color" data-color="<?php the_sub_field('zr-konstruktor_izobrazhenie_derevo'); ?>" style="background: url(<?php the_sub_field('zr-konstruktor_izobrazhenie_derevo'); ?>) no-repeat center center/cover;"></div>
												  <p class="choose-item__text"><?php the_sub_field('zr-konstruktor_imya_czveta_derevo'); ?></p>
											  </div>
										  <?php endwhile; ?>
										  <?php endif; ?>
										  </ul>
									  </div>
								  <?php endwhile; ?>
								  <?php endif; ?>
  
								  </div>
							  </div>
						  </div>
					  </div>
			  </section>
			  <?php } ?>
  <?php endwhile; } ?>
			  
  
		  </main>
  
  
  
  
  
  
	  
		  <script defer="defer" src="<?php echo get_stylesheet_directory_uri() ?>/js/main.bundle.js"></script>
		  <script defer="defer" src="<?php echo get_stylesheet_directory_uri() ?>/js/gruby_script.js"></script>
		  <?php wp_footer(); ?>
	  </body> 
  </html>