<?php get_header(); ?>


<main id="category-page">
			<section class="category-main-section">
                        
				<div class="section-bg"><img src="<?php $category = get_queried_object(); $image = get_field('category_image', $category); echo $image; ?>" alt="background" /></div>
				<div class="container">
					<div class="breadcrumb" data-aos="show-up-20">
						<a class="breadcrumb-item" href="/">Главная</a>
						<a class="breadcrumb-item" href="<?php echo get_category_link( $category_id ); ?>"><?php single_term_title(); ?></a>
					</div>
					<div class="row">
						<div class="col-12 col-lg-6">
							<h1 class="section-title" data-aos="show-up-20" data-aos-delay="200"><?php single_term_title(); ?></h1>
							<p class="section-sub-title" data-aos="show-up-20" data-aos-delay="400"><?php if ( '' != the_archive_description() ) { echo esc_html( the_archive_description() ); } ?></p>
							<button
								class="custom-btn secondary"
								type="button"
								data-toggle="modal"
								data-target="#feedback"
								data-aos="show-up-20"
								data-aos-delay="600"
							>
								<span>Получить консультацию</span>
							</button>
						</div>
						<div class="col-12 offset-lg-2 col-lg-4">
							<div class="sub-categories" data-aos="show-up-20" data-aos-delay="400">

                            <?php 
                            
                            $termID = get_queried_object()->term_id; // get_queried_object()->term_id; - динамическое получение ID текущей рубрики
                            $taxonomyName = "category";
                            $termchildren = get_term_children( $termID, $taxonomyName );

                            foreach ($termchildren as $child) {
                                $term = get_term_by( 'id', $child, $taxonomyName );
                                echo '<a class="sub-category-link" href="' . get_term_link( $term->term_id, $term->taxonomy ) . '"><span>' . $term->name . '</span></a>';
                            } ?> 
                            
                            </div>
						</div>
					</div>
				</div>
			</section>
			<section class="sub-category-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-6 p-0">
							<div class="sub-category__image" data-aos="clip-right"><img src="<?php the_field('cat_block_img1', 'option'); ?>" alt="<?php the_field('cat_block_title1', 'option'); ?>" /></div>
						</div>
						<div class="col-12 col-lg-6">
							<div class="sub-category__info">
								<h3 class="sub-category__title" data-aos="show-up-20" data-aos-delay="200"><?php the_field('cat_block_title1', 'option'); ?></h3>
								<p class="sub-category__desc" data-aos="show-up-20" data-aos-delay="400"><?php the_field('cat_block_descr1', 'option'); ?></p>
								<a class="custom-btn secondary" href="<?php the_field('cat_block_link1', 'option'); ?>" data-aos="show-up-20" data-aos-delay="600"><span><?php the_field('cat_block_button1', 'option'); ?></span></a>
							</div>
						</div>
						<div class="col-12 col-advantages">
							<div class="row">
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="600">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_1.svg" alt="<?php the_field('incat_title1', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title1', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content1', 'option'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="800">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_2.svg" alt="<?php the_field('incat_title2', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title2', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content2', 'option'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="1000">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_3.svg" alt="<?php the_field('incat_title3', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title3', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content3', 'option'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="1200">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_4.svg" alt="<?php the_field('incat_title4', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title4', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content4', 'option'); ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="sub-category-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-6 p-0">
							<div class="sub-category__image" data-aos="clip-right"><img src="<?php the_field('cat_block_img2', 'option'); ?>" alt="<?php the_field('cat_block_title2', 'option'); ?>" /></div>
						</div>
						<div class="col-12 col-lg-6">
							<div class="sub-category__info">
								<h3 class="sub-category__title" data-aos="show-up-20" data-aos-delay="200"><?php the_field('cat_block_title2', 'option'); ?></h3>
								<p class="sub-category__desc" data-aos="show-up-20" data-aos-delay="400"><?php the_field('cat_block_descr2', 'option'); ?></p>
								<a class="custom-btn secondary" href="<?php the_field('cat_block_link2', 'option'); ?>" data-aos="show-up-20" data-aos-delay="600"><span><?php the_field('cat_block_button2', 'option'); ?></span></a>
							</div>
						</div>
						<div class="col-12 col-advantages">
							<div class="row">
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="600">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_1.svg" alt="<?php the_field('incat_title11', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title11', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content11', 'option'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="800">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_2.svg" alt="<?php the_field('incat_title22', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title22', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content22', 'option'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="1000">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_3.svg" alt="<?php the_field('incat_title33', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title33', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content33', 'option'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="1200">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_4.svg" alt="<?php the_field('incat_title44', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title44', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content44', 'option'); ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="sub-category-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-6 p-0">
							<div class="sub-category__image" data-aos="clip-right"><img src="<?php the_field('cat_block_img3', 'option'); ?>" alt="<?php the_field('cat_block_title3', 'option'); ?>" /></div>
						</div>
						<div class="col-12 col-lg-6">
							<div class="sub-category__info">
								<h3 class="sub-category__title" data-aos="show-up-20" data-aos-delay="200"><?php the_field('cat_block_title3', 'option'); ?></h3>
								<p class="sub-category__desc" data-aos="show-up-20" data-aos-delay="400"><?php the_field('cat_block_descr3', 'option'); ?></p>
								<a class="custom-btn secondary" href="<?php the_field('cat_block_link3', 'option'); ?>" data-aos="show-up-20" data-aos-delay="600"><span><?php the_field('cat_block_button3', 'option'); ?></span></a>
							</div>
						</div>
						<div class="col-12 col-advantages">
							<div class="row">
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="600">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_1.svg" alt="<?php the_field('incat_title111', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title111', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content111', 'option'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="800">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_2.svg" alt="<?php the_field('incat_title222', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title222', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content222', 'option'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="1000">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_3.svg" alt="<?php the_field('incat_title333', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title333', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content333', 'option'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="1200">
										<div class="advantage-card__icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/sub_advantage_4.svg" alt="<?php the_field('incat_title444', 'option'); ?>" /></div>
										<h4 class="advantage-card__title"><?php the_field('incat_title444', 'option'); ?></h4>
										<p class="advantage-card__desc"><?php the_field('incat_content444', 'option'); ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="consultation-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-7">
							<div class="consultation-section__info" data-aos="clip-right">
								<h2 class="consultation-section__title" data-aos="show-up-20" data-aos-delay="400"><?php the_field('consultation_title', 'option'); ?></h2>
								<p class="consultation-section__desc" data-aos="show-up-20" data-aos-delay="600"><?php the_field('consultation_descr', 'option'); ?></p>
								<div data-aos="show-up-20" data-aos-delay="600">
									<a class="consultation-section__phone" href="tel:<?php the_field('consultation_phone', 'option'); ?>"><?php the_field('consultation_phone', 'option'); ?></a>
								</div>
								<button
									class="custom-btn inverse"
									type="button"
									data-toggle="modal"
									data-target="#feedback"
									data-aos="show-up-20"
									data-aos-delay="800"
								>
									<span><?php the_field('consultation_button', 'option'); ?></span>
								</button>
							</div>
						</div>
						<div class="col-12 col-lg-5">
							<div class="consultation-section__image" data-aos="clip-right" data-aos-delay="400">
								<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/default.jpeg" alt="consultation" />
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>

<?php get_footer(); ?>