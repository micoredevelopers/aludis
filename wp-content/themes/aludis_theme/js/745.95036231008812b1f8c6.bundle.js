(self["webpackChunk"] = self["webpackChunk"] || []).push([[745],{

/***/ 745:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "initStockSlider": () => /* binding */ initStockSlider
/* harmony export */ });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(755);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
;

function initMainSlider() {
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('#main-slider').slick({
    speed: 700,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
  });
}

function initStockSlider() {
  var $stockSlider = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#stock-slider');
  var $currentSlide = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.stock-slider').find('.slider-count_current');
  var $totalSlides = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.stock-slider').find('.slider-count_total');
  $stockSlider.on('init', function (e, slick) {
    $totalSlides.text(slick.slideCount);
    $currentSlide.text(slick.currentSlide + 1);
  });
  $stockSlider.slick({
    speed: 500,
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '.stock-slider .slider-arrow_prev',
    nextArrow: '.stock-slider .slider-arrow_next',
    responsive: [{
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  $stockSlider.on('beforeChange', function (e, slick, current, next) {
    $currentSlide.text(next + 1);
  });
}

function initReviewsSlider() {
  var $reviewsSlider = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#reviews-slider');
  var $currentSlide = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.reviews-slider').find('.slider-count_current');
  var $totalSlides = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.reviews-slider').find('.slider-count_total');
  $reviewsSlider.on('init', function (e, slick) {
    $totalSlides.text(slick.slideCount);
    $currentSlide.text(slick.currentSlide + 1);
  });
  $reviewsSlider.slick({
    speed: 700,
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '.reviews-slider .slider-arrow_prev',
    nextArrow: '.reviews-slider .slider-arrow_next',
    responsive: [{
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  $reviewsSlider.on('beforeChange', function (e, slick, current, next) {
    $currentSlide.text(next + 1);
  });
}

jquery__WEBPACK_IMPORTED_MODULE_0___default()(document).ready(function () {
  initMainSlider();
  initStockSlider();
  initReviewsSlider();
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('.review-card__see-more').on('click', function () {
    if (jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).hasClass('opened')) {
      jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).text('развернуть');
    } else {
      jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).text('свернуть');
    }

    jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).toggleClass('opened');
    jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).parents('.review-card').toggleClass('opened');
    jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).parent().find('.review-card__desc').toggleClass('d-block');
  });
});

/***/ })

}]);