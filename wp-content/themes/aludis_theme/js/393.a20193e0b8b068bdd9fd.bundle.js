(self["webpackChunk"] = self["webpackChunk"] || []).push([[393],{

/***/ 393:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(755);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
;

function initStockSlider() {
  var $stockSlider = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#stock-slider');
  var $currentSlide = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.stock-slider').find('.slider-count_current');
  var $totalSlides = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.stock-slider').find('.slider-count_total');
  $stockSlider.on('init', function (e, slick) {
    $totalSlides.text(slick.slideCount);
    $currentSlide.text(slick.currentSlide + 1);
  });
  $stockSlider.slick({
    speed: 500,
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '.stock-slider .slider-arrow_prev',
    nextArrow: '.stock-slider .slider-arrow_next',
    responsive: [{
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  $stockSlider.on('beforeChange', function (e, slick, current, next) {
    $currentSlide.text(next + 1);
  });
}

function initExampleSlider() {
  var $exampleSlider = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#example-slider');
  var $currentSlide = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.example-slider').find('.slider-count_current');
  var $totalSlides = jquery__WEBPACK_IMPORTED_MODULE_0___default()('.example-slider').find('.slider-count_total');
  $exampleSlider.on('init', function (e, slick) {
    slick.$slides.each(function (index, slide) {
      var $innerSlider = jquery__WEBPACK_IMPORTED_MODULE_0___default()(slide).find("#example-inner-slider_".concat(index + 1));
      $innerSlider.slick({
        speed: 600,
        draggable: false,
        swipe: false
      });
    });

    if (!jquery__WEBPACK_IMPORTED_MODULE_0___default()(slick.$slider).attr('id').includes('inner')) {
      if (window.innerWidth < 991) {
        $totalSlides.text(slick.slideCount);
      } else {
        $totalSlides.text(Math.ceil(slick.slideCount / slick.options.slidesToShow));
      }

      $currentSlide.text(slick.currentSlide + 1);
    } // if (slick.currentSlide === slick.defaults.initialSlide) {
    //   slick.$prevArrow.addClass('d-none')
    // }

  });
  $exampleSlider.slick({
    speed: 600,
    dots: false,
    slidesToShow: 3,
    infinite: false,
    slidesToScroll: 1,
    prevArrow: '.example-slider .slider-arrow_prev',
    nextArrow: '.example-slider .slider-arrow_next',
    responsive: [{
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  $exampleSlider.on('beforeChange', function (e, slick, current, next) {
    if (!jquery__WEBPACK_IMPORTED_MODULE_0___default()(slick.$slider).attr('id').includes('inner')) {
      $currentSlide.text(next + 1);
    } // if (next !== slick.defaults.initialSlide) {
    //   slick.$prevArrow.removeClass('d-none')
    // } else {
    //   slick.$prevArrow.addClass('d-none')
    // }
    //
    // if (next + 1 === slick.slideCount) {
    //   slick.$nextArrow.addClass('d-none')
    // } else {
    //   slick.$nextArrow.removeClass('d-none')
    // }

  });
}

jquery__WEBPACK_IMPORTED_MODULE_0___default()(document).ready(function () {
  initStockSlider();
  initExampleSlider();
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('.choose-item[data-type]').on('click', function () {
    var type = jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).attr('data-type');
    jquery__WEBPACK_IMPORTED_MODULE_0___default()(".choose-item[data-type=".concat(type, "]")).removeClass('active');
    jquery__WEBPACK_IMPORTED_MODULE_0___default()(this).toggleClass('active');
  });
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('.choose-item__color').each(function (index, item) {
    var color = jquery__WEBPACK_IMPORTED_MODULE_0___default()(item).attr('data-color');
    jquery__WEBPACK_IMPORTED_MODULE_0___default()(item).css('background-color', color);
  });
});

/***/ })

}]);