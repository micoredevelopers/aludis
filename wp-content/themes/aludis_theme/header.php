<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
		<meta http-equiv="X-UA-Compatible" content="ie=edge, chrome=1" />
        <?php wp_head(); ?>
		<title>Aludis</title>
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri() ?>/favicon.ico" />

        <link href="<?php echo get_stylesheet_directory_uri() ?>/styles/main.css" rel="stylesheet" />
    </head>
	<body <?php body_class(); ?>>
		<header class="header">
			<div class="container">
				<div class="header__row">
					<div class="header__actions">
						<div class="header__menu d-flex d-lg-none" data-aos="show-up-20" data-aos-delay="200">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</div>
						<a class="header__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" data-aos="show-up-20" data-aos-delay="400"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo.svg" alt="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" /></a>
						<ul class="header__links-list d-none d-lg-flex" data-aos="show-up-20" data-aos-delay="600">
                        <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'items_wrap' => '%3$s', 'container' => '', 'link_before' => '<span>', 'link_after' => '</span>', 'walker' => new My_Walker_Nav_Menu() )
                            ); ?>
							
						</ul>
						<a class="header__call d-flex d-lg-none" href="tel: <?php the_field('phone_field', 'option'); ?>" data-aos="show-up-20" data-aos-delay="600">
							<img class="svg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/call_mobile.svg" alt="Feedback" />
						</a>
					</div>
					<div class="header__actions d-none d-lg-flex">
						<a class="header__call" href="mailto:<?php the_field('email_field', 'option'); ?>" data-aos="show-up-20" data-aos-delay="800">
							<img class="svg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/mail.svg" alt="Mail" />
							<span><?php the_field('email_field', 'option'); ?></span>
						</a>
						<a class="header__call" href="tel: <?php the_field('phone_field', 'option'); ?>" data-aos="show-up-20" data-aos-delay="1000">
							<img class="svg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/call.svg" alt="Mail" />
							<span><?php the_field('phone_field', 'option'); ?></span>
						</a>
					</div>
				</div>
			</div>
		</header>