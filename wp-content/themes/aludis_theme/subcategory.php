<?php get_header(); ?>
 
<main id="sub-category-page">
			<section class="category-main-section">
				<div class="section-bg"><img src="/assets/images/default.jpeg" alt="background" /></div>
				<div class="container">
					<div class="breadcrumb" data-aos="show-up-20">
						<a class="breadcrumb-item" href="/">Главная</a>
						<a class="breadcrumb-item" href="/category.html">Автоматические ворота312</a>
						<a class="breadcrumb-item" href="<?php echo get_category_link( $category_id ); ?>"><?php single_term_title(); ?></a>
					</div>
					<div class="row">
						<div class="col-12 col-lg-6">
							<h1 class="section-title" data-aos="show-up-20" data-aos-delay="200"><?php single_term_title(); ?></h1>
							<p class="section-sub-title" data-aos="show-up-20" data-aos-delay="400"><?php if ( '' != the_archive_description() ) { echo esc_html( the_archive_description() ); } ?></p>
							<button
								class="custom-btn secondary"
								type="button"
								data-toggle="modal"
								data-target="#feedback"
								data-aos="show-up-20"
								data-aos-delay="600"
							>
								<span>Получить консультацию</span>
							</button>
						</div>
						<div class="col-12 offset-lg-2 col-lg-4">
							<div class="sub-categories" data-aos="show-up-20" data-aos-delay="400">


                            <?php

$term = get_queried_object();

$neighbors = get_terms( [
    'taxonomy'   => $term->taxonomy,
    'parent'     => $term->parent,
    'hide_empty' => false
] );

if ( $neighbors ) { 
    foreach( $neighbors as $subcat )
    {
        echo '<a class="sub-category-link" href="' . esc_url(get_term_link($subcat, $subcat->taxonomy)) . '"><span>' . $subcat->name . '</span></a>';
                           
    }
}
?>

							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="home-stock-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-2"><h2 class="section-title" data-aos="show-up-20">Акции</h2></div>
						<div class="col-12 col-lg-10">
							<div class="slider-wrap stock-slider">
								<div class="slider-nav" data-aos="show-up-20" data-aos-delay="200">
									<div class="slider-arrow slider-arrow_prev"><img src="/assets/images/slider_arrow.svg" alt="Prev" /></div>
									<div class="slider-count">
										<p class="slider-count__text slider-count_current">1</p>
										<p class="slider-count__text">/</p>
										<p class="slider-count__text slider-count_total">2</p>
									</div>
									<div class="slider-arrow slider-arrow_next"><img src="/assets/images/slider_arrow.svg" alt="Next" /></div>
								</div>
								<div class="slider" id="stock-slider">
									<div class="slide" data-aos="show-up-20" data-aos-delay="400">
										<div class="stock-slider__image"><img src="/assets/images/default.jpeg" alt="Название акции 1" /></div>
										<h4 class="stock-slider__title">Название акции 1</h4>
										<p class="stock-slider__desc">Audio player software is used to play back sound recordings in one of the many formats</p>
										<div class="stock-slider__price-wrap">
											<p class="stock-slider__price stock-slider__price_new">19900 р</p>
											<p class="stock-slider__price stock-slider__price_old">23000 р</p>
										</div>
										<button class="custom-btn" type="button" data-toggle="modal" data-target="#feedback"><span>Узнать детали</span></button>
									</div>
									<div class="slide" data-aos="show-up-20" data-aos-delay="600">
										<div class="stock-slider__image"><img src="/assets/images/default.jpeg" alt="Название акции 1" /></div>
										<h4 class="stock-slider__title">Название акции 1</h4>
										<p class="stock-slider__desc">Audio player software is used to play back sound recordings in one of the many formats</p>
										<div class="stock-slider__price-wrap">
											<p class="stock-slider__price stock-slider__price_new">19900 р</p>
											<p class="stock-slider__price stock-slider__price_old">23000 р</p>
										</div>
										<button class="custom-btn" type="button" data-toggle="modal" data-target="#feedback"><span>Узнать детали</span></button>
									</div>
									<div class="slide" data-aos="show-up-20" data-aos-delay="800">
										<div class="stock-slider__image"><img src="/assets/images/default.jpeg" alt="Название акции 1" /></div>
										<h4 class="stock-slider__title">Название акции 1</h4>
										<p class="stock-slider__desc">Audio player software is used to play back sound recordings in one of the many formats</p>
										<div class="stock-slider__price-wrap">
											<p class="stock-slider__price stock-slider__price_new">19900 р</p>
											<p class="stock-slider__price stock-slider__price_old">23000 р</p>
										</div>
										<button class="custom-btn" type="button" data-toggle="modal" data-target="#feedback"><span>Узнать детали</span></button>
									</div>
									<div class="slide" data-aos="show-up-20" data-aos-delay="1000">
										<div class="stock-slider__image"><img src="/assets/images/default.jpeg" alt="Название акции 1" /></div>
										<h4 class="stock-slider__title">Название акции 1</h4>
										<p class="stock-slider__desc">Audio player software is used to play back sound recordings in one of the many formats</p>
										<div class="stock-slider__price-wrap">
											<p class="stock-slider__price stock-slider__price_new">19900 р</p>
											<p class="stock-slider__price stock-slider__price_old">23000 р</p>
										</div>
										<button class="custom-btn" type="button" data-toggle="modal" data-target="#feedback"><span>Узнать детали</span></button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="sub-category-equal-section">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="row">
								<div class="col-12 col-lg-4 col-title">
									<h2 class="section-title" data-aos="show-up-20">Сравнение секционных и роллетных ворот «АЛЮТЕХ»</h2>
									<button
										class="custom-btn secondary"
										type="button"
										data-toggle="modal"
										data-target="#feedback"
										data-aos="show-up-20"
										data-aos-delay="200"
									>
										<span>Получить консультацию</span>
									</button>
								</div>
								<div class="col-12 col-lg-8">
									<div class="row">
										<div class="col-12 col-lg-6">
											<div class="equal-card" data-aos="show-up-20" data-aos-delay="400">
												<div class="equal-card__image"><img src="/assets/images/default.jpeg" alt="equal" /></div>
												<h3 class="equal-card__title">Секционные ворота</h3>
												<p class="equal-card__desc">Теплые ворота в любую пору года.</p>
											</div>
										</div>
										<div class="col-12 col-lg-6 d-block d-lg-none">
											<div class="equal-parameter-wrap">
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="0">
													<p class="equal-parameter__title">Принцип действия</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Полотно из сэндвич-панелей</span>
													</p>
													<p class="equal-parameter__desc">
														Перемещается по направляющим профилям из вертикального в горизонтальное положение и обратно. В открытом
														положении полотно размещается под потолком.
													</p>
												</div>
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="200">
													<p class="equal-parameter__title">Энергоэффективность</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Высокая</span>
													</p>
													<p class="equal-parameter__desc">
														Полотно ворот изготовлено из сэндвич-панелей, толщиной 40-45 мм, что сопоставимо по теплоизоляции с
														кирпичной стеной толщиной 55-60 см.
													</p>
												</div>
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="400">
													<p class="equal-parameter__title">Долговечность</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Высокая</span>
													</p>
													<p class="equal-parameter__desc">
														Трехслойное покрытие панелей гарантирует надежную защиту от коррозии, а верхний слой - декоративное
														покрытие ПУР-ПА, обладает высокой стойкостью к повреждениям, перепадам температуры, выгоранию на солнце.
														10 лет гарантии от сквозной коррозии.
													</p>
												</div>
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="600">
													<p class="equal-parameter__title">Герметичность</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Высокая</span>
													</p>
													<p class="equal-parameter__desc">
														По периметру ворот устанавливаются эластичные, несминаемые и устойчивые к низким температурам
														уплотнители EPDM.
													</p>
												</div>
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="600">
													<p class="equal-parameter__title">Ветроустойчивость</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Высокая</span>
													</p>
													<button class="custom-btn secondary" type="button" data-toogle="modal" data-target="#feedback">
														<span>Узнать детали</span>
													</button>
												</div>
											</div>
										</div>
										<div class="col-12 col-lg-6">
											<div class="equal-card" data-aos="show-up-20" data-aos-delay="600">
												<div class="equal-card__image"><img src="/assets/images/default.jpeg" alt="equal" /></div>
												<h3 class="equal-card__title">Секционные ворота</h3>
												<p class="equal-card__desc">Теплые ворота в любую пору года.</p>
											</div>
										</div>
										<div class="col-12 col-lg-6 d-block d-lg-none">
											<div class="equal-parameter-wrap">
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="NaN">
													<p class="equal-parameter__title">Принцип действия</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Полотно из сэндвич-панелей</span>
													</p>
													<p class="equal-parameter__desc">
														Перемещается по направляющим профилям из вертикального в горизонтальное положение и обратно. В открытом
														положении полотно размещается под потолком.
													</p>
												</div>
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="NaN">
													<p class="equal-parameter__title">Энергоэффективность</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Высокая</span>
													</p>
													<p class="equal-parameter__desc">
														Полотно ворот изготовлено из сэндвич-панелей, толщиной 40-45 мм, что сопоставимо по теплоизоляции с
														кирпичной стеной толщиной 55-60 см.
													</p>
												</div>
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="NaN">
													<p class="equal-parameter__title">Долговечность</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Высокая</span>
													</p>
													<p class="equal-parameter__desc">
														Трехслойное покрытие панелей гарантирует надежную защиту от коррозии, а верхний слой - декоративное
														покрытие ПУР-ПА, обладает высокой стойкостью к повреждениям, перепадам температуры, выгоранию на солнце.
														10 лет гарантии от сквозной коррозии.
													</p>
												</div>
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="NaN">
													<p class="equal-parameter__title">Герметичность</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Высокая</span>
													</p>
													<p class="equal-parameter__desc">
														По периметру ворот устанавливаются эластичные, несминаемые и устойчивые к низким температурам
														уплотнители EPDM.
													</p>
												</div>
												<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="600">
													<p class="equal-parameter__title">Ветроустойчивость</p>
													<p class="equal-parameter__sub-title">
														<img src="/assets/images/success.svg" alt="success" />
														<span>Высокая</span>
													</p>
													<button class="custom-btn secondary" type="button" data-toogle="modal" data-target="#feedback">
														<span>Узнать детали</span>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12 d-none d-lg-block">
									<div class="row equal-row">
										<div class="col-4">
											<div class="equal-parameter" data-aos="show-up-20"><p class="equal-parameter__title">Принцип действия</p></div>
										</div>
										<div class="col-4">
											<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="200">
												<p class="equal-parameter__sub-title">
													<img src="/assets/images/success.svg" alt="success" />
													<span>Полотно из сэндвич-панелей</span>
												</p>
												<p class="equal-parameter__desc">
													Перемещается по направляющим профилям из вертикального в горизонтальноеположение и обратно. В открытом
													положении полотно размещается под потолком.
												</p>
											</div>
										</div>
										<div class="col-4">
											<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="400">
												<p class="equal-parameter__sub-title">
													<img src="/assets/images/failure.svg" alt="failure" />
													<span>Стандартная</span>
												</p>
											</div>
										</div>
									</div>
									<div class="row equal-row">
										<div class="col-4">
											<div class="equal-parameter" data-aos="show-up-20"><p class="equal-parameter__title">Ветроустойчивость</p></div>
										</div>
										<div class="col-4">
											<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="200">
												<p class="equal-parameter__sub-title">
													<img src="/assets/images/success.svg" alt="success" />
													<span>Высокая</span>
												</p>
												<button class="custom-btn secondary" type="button" data-toggle="modal" data-target="#feedback">
													<span>Узнать детали</span>
												</button>
											</div>
										</div>
										<div class="col-4">
											<div class="equal-parameter" data-aos="show-up-20" data-aos-delay="400">
												<p class="equal-parameter__sub-title">
													<img src="/assets/images/failure.svg" alt="failure" />
													<span>Стандартная</span>
												</p>
												<button class="custom-btn secondary" type="button" data-toggle="modal" data-target="#feedback">
													<span>Узнать детали</span>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="sub-category-appearance-section" data-aos="show-up-20">
				<div class="container">
					<h2 class="section-title">Внешний вид</h2>
					<div class="row">
						<div class="col-12 col-lg-6 col-appearance">
							<div class="choose-image-wrap"><img src="/assets/images/default.jpeg" alt="appearance" /></div>
							<a class="choose-link" href="/">Узнать стоимость выбранного варианта</a>
						</div>
						<div class="col-12 col-lg-6">
							<div class="choose-type-wrap">
								<p class="choose-title">Типы профиля</p>
								<ul class="nav nav-tab">
									<li class="nav-item">
										<a class="choose-item type-item active" data-toggle="tab" href="#tab-1">
											<div class="choose-item__image"><img src="/assets/images/default.jpeg" alt="type" /></div>
											<span class="choose-item__text">S-гофр</span>
										</a>
									</li>
									<li class="nav-item">
										<a class="choose-item type-item" data-toggle="tab" href="#tab-2">
											<div class="choose-item__image"><img src="/assets/images/default.jpeg" alt="type" /></div>
											<span class="choose-item__text">Микроволна</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="choose-type-wrap">
								<p class="choose-title">Палитра</p>
								<div class="tab-content">
									<div class="tab-pane fade show active" id="tab-1">
										<p class="choose-sub-title">Стандартная</p>
										<ul class="choose-items">
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#e3d1b5"></div>
												<p class="choose-item__text">RAL 1015</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#701f29"></div>
												<p class="choose-item__text">RAL 3004</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#14447c"></div>
												<p class="choose-item__text">RAL 5010</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#0e4336"></div>
												<p class="choose-item__text">RAL 6005</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#373f43"></div>
												<p class="choose-item__text">RAL 7016</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#4a3526"></div>
												<p class="choose-item__text">RAL 8014</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#a6a8a6"></div>
												<p class="choose-item__text">RAL 9006</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#f7fbf6"></div>
												<p class="choose-item__text">RAL 9016</p>
											</div>
										</ul>
									</div>
									<div class="tab-pane fade show" id="tab-2">
										<p class="choose-sub-title">Стандартная</p>
										<ul class="choose-items">
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#e3d1b5"></div>
												<p class="choose-item__text">RAL 1015</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#701f29"></div>
												<p class="choose-item__text">RAL 3004</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#14447c"></div>
												<p class="choose-item__text">RAL 5010</p>
											</div>
											<div class="choose-item palette-item" data-type="color">
												<div class="choose-item__color" data-color="#0e4336"></div>
												<p class="choose-item__text">RAL 6005</p>
											</div>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="consultation-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-7">
							<div class="consultation-section__info" data-aos="clip-right">
								<h2 class="consultation-section__title" data-aos="show-up-20" data-aos-delay="400">Нужна консультация?</h2>
								<p class="consultation-section__desc" data-aos="show-up-20" data-aos-delay="600">
									Наши консультанты помогут с выбором и назовут точную стоимость. Звоните:
								</p>
								<div data-aos="show-up-20" data-aos-delay="600">
									<a class="consultation-section__phone" href="tel:+38 (096) 123 45 67">+38 (096) 123 45 67</a>
								</div>
								<button
									class="custom-btn inverse"
									type="button"
									data-toggle="modal"
									data-target="#feedback"
									data-aos="show-up-20"
									data-aos-delay="800"
								>
									<span>Перезвоните мне</span>
								</button>
							</div>
						</div>
						<div class="col-12 col-lg-5">
							<div class="consultation-section__image" data-aos="clip-right" data-aos-delay="400">
								<img src="/assets/images/default.jpeg" alt="consultation" />
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="sub-category-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-6 p-0">
							<div class="sub-category__image" data-aos="clip-right"><img src="/assets/images/default.jpeg" alt="subcategory" /></div>
						</div>
						<div class="col-12 col-lg-6">
							<div class="sub-category__info">
								<h3 class="sub-category__title" data-aos="show-up-20" data-aos-delay="200">Уточнение границ земельного участка</h3>
								<p class="sub-category__desc" data-aos="show-up-20" data-aos-delay="400">
									Для современного мира семантический разбор внешних противодействий является качественно новой ступенью своевременного
									выполнения сверхзадачи. Для современного мира семантический разбор внешних противодействий является качественно новой
									ступенью своевременного выполнения сверхзадачи.
								</p>
								<a class="custom-btn secondary" href="/sub-category.html" data-aos="show-up-20" data-aos-delay="600"><span>Подробнее</span></a>
							</div>
						</div>
						<div class="col-12 col-advantages">
							<div class="row">
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="600">
										<div class="advantage-card__icon"><img src="/assets/images/sub_advantage_1.svg" alt="Небольшие сроки" /></div>
										<h4 class="advantage-card__title">Небольшие сроки</h4>
										<p class="advantage-card__desc">
											Объединение Польши и Литвы в одно государство сделало возможным их совместные контрнаступательные действия. России с
											трудом удалось отстоять Псков.
										</p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="800">
										<div class="advantage-card__icon"><img src="/assets/images/sub_advantage_2.svg" alt="Оптимальная стоимость" /></div>
										<h4 class="advantage-card__title">Оптимальная стоимость</h4>
										<p class="advantage-card__desc">
											Объединение Польши и Литвы в одно государство сделало возможным их совместные контрнаступательные действия. России с
											трудом удалось отстоять Псков.
										</p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="1000">
										<div class="advantage-card__icon"><img src="/assets/images/sub_advantage_3.svg" alt="Оформление документов" /></div>
										<h4 class="advantage-card__title">Оформление документов</h4>
										<p class="advantage-card__desc">
											Объединение Польши и Литвы в одно государство сделало возможным их совместные контрнаступательные действия. России с
											трудом удалось отстоять Псков.
										</p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="1200">
										<div class="advantage-card__icon"><img src="/assets/images/sub_advantage_4.svg" alt="Работаем 24/7" /></div>
										<h4 class="advantage-card__title">Работаем 24/7</h4>
										<p class="advantage-card__desc">
											Объединение Польши и Литвы в одно государство сделало возможным их совместные контрнаступательные действия. России с
											трудом удалось отстоять Псков.
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-examples">
							<div class="row">
								<div class="col-12">
									<div class="row">
										<div class="col-12 col-lg-7"><h3 class="section-title" data-aos="show-up-20">Популярные готовые решения</h3></div>
										<div class="col-12 col-lg-4 offset-lg-1">
											<p class="section-sub-title" data-aos="show-up-20" data-aos-delay="200">
												Для современного мира семантический разбор внешних противодействий является качественно новой ступенью
												своевременного выполнения сверхзадачи.
											</p>
										</div>
									</div>
								</div>
								<div class="col-12">
									<div class="slider-wrap example-slider">
										<div class="slider" id="example-slider" data-aos="show-up-20" data-aos-delay="400">
											<div class="slide example-slide">
												<div class="example-inner-slider slider" id="example-inner-slider_1">
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
												</div>
												<div class="example-card">
													<p class="example-card__title">Роллетные ворота серии Prestige 1</p>
													<p class="example-card__desc">Воротный профиль AG/77, высокая защита от взлома</p>
													<p class="example-card__parameters">
														цвет: коричневый, 2600×2100 мм, с автоматическим управлением, накладной монтаж, защитный короб 45°
													</p>
													<button class="custom-btn secondary" type="button" data-toggle="modal" data-target="#feedback">
														<span>Узнать стоимость</span>
													</button>
												</div>
											</div>
											<div class="slide example-slide">
												<div class="example-inner-slider slider" id="example-inner-slider_2">
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
												</div>
												<div class="example-card">
													<p class="example-card__title">Роллетные ворота серии Prestige 2</p>
													<p class="example-card__desc">Воротный профиль AG/77, высокая защита от взлома</p>
													<p class="example-card__parameters">
														цвет: коричневый, 2600×2100 мм, с автоматическим управлением, накладной монтаж, защитный короб 45°
													</p>
													<button class="custom-btn secondary" type="button" data-toggle="modal" data-target="#feedback">
														<span>Узнать стоимость</span>
													</button>
												</div>
											</div>
											<div class="slide example-slide">
												<div class="example-inner-slider slider" id="example-inner-slider_3">
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
												</div>
												<div class="example-card">
													<p class="example-card__title">Роллетные ворота серии Prestige 3</p>
													<p class="example-card__desc">Воротный профиль AG/77, высокая защита от взлома</p>
													<p class="example-card__parameters">
														цвет: коричневый, 2600×2100 мм, с автоматическим управлением, накладной монтаж, защитный короб 45°
													</p>
													<button class="custom-btn secondary" type="button" data-toggle="modal" data-target="#feedback">
														<span>Узнать стоимость</span>
													</button>
												</div>
											</div>
											<div class="slide example-slide">
												<div class="example-inner-slider slider" id="example-inner-slider_4">
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="/assets/images/default.jpeg" alt="example" /></div>
													</div>
												</div>
												<div class="example-card">
													<p class="example-card__title">Роллетные ворота серии Prestige 4</p>
													<p class="example-card__desc">Воротный профиль AG/77, высокая защита от взлома</p>
													<p class="example-card__parameters">
														цвет: коричневый, 2600×2100 мм, с автоматическим управлением, накладной монтаж, защитный короб 45°
													</p>
													<button class="custom-btn secondary" type="button" data-toggle="modal" data-target="#feedback">
														<span>Узнать стоимость</span>
													</button>
												</div>
											</div>
										</div>
										<div class="slider-nav" data-aos="show-up-20" data-aos-delay="600">
											<div class="slider-arrow slider-arrow_prev"><span>назад</span></div>
											<div class="slider-count">
												<p class="slider-count__text slider-count_current">1</p>
												<p class="slider-count__text">/</p>
												<p class="slider-count__text slider-count_total">2</p>
											</div>
											<div class="slider-arrow slider-arrow_next"><span>далее</span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="sub-category-video-section">
				<div class="container">
					<h1 class="section-title" data-aos="show-up-20">Очень увлекательное видео</h1>
					<div class="video-wrap" data-aos="show-up-20" data-aos-delay="200">
						<iframe
							src="https://www.youtube.com/embed/W7hAo28NCXc"
							frameborder="0"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
							allowfullscreen
						></iframe>
					</div>
				</div>
			</section>
			<section class="consultation-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-7">
							<div class="consultation-section__info" data-aos="clip-right">
								<h2 class="consultation-section__title" data-aos="show-up-20" data-aos-delay="400">Нужна консультация?</h2>
								<p class="consultation-section__desc" data-aos="show-up-20" data-aos-delay="600">
									Наши консультанты помогут с выбором и назовут точную стоимость. Звоните:
								</p>
								<div data-aos="show-up-20" data-aos-delay="600">
									<a class="consultation-section__phone" href="tel:+38 (096) 123 45 67">+38 (096) 123 45 67</a>
								</div>
								<button
									class="custom-btn inverse"
									type="button"
									data-toggle="modal"
									data-target="#feedback"
									data-aos="show-up-20"
									data-aos-delay="800"
								>
									<span>Перезвоните мне</span>
								</button>
							</div>
						</div>
						<div class="col-12 col-lg-5">
							<div class="consultation-section__image" data-aos="clip-right" data-aos-delay="400">
								<img src="/assets/images/default.jpeg" alt="consultation" />
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>

<?php get_footer(); ?>