<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-4 col-lg-2">
						<div class="footer__content">
							<a class="footer__logo" href="/" data-aos="show-up-20"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo_footer.svg" alt="Logo" /></a>
							<div class="footer__socials-wrap d-flex d-lg-none" data-aos="show-up-20" data-aos-delay="200">
								<a class="footer__social" href="/"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/instagram.svg" alt="instagram" /></a>
								<a class="footer__social" href="/"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/facebook.svg" alt="facebook" /></a>
							</div>
						</div>
					</div>
					<div class="col-3 d-none d-lg-block">
						<div class="footer__links-wrap" data-aos="show-up-20" data-aos-delay="400">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'items_wrap' => '%3$s', 'container' => '', 'link_before' => '', 'link_after' => '', 'walker' => new Description_Walker )
                            ); ?>
						</div>
					</div>
					<div class="col-8 col-lg-5">
						<div class="footer__contacts-wrap">
							<div class="footer__content" data-aos="show-up-20" data-aos-delay="600">
								<a class="footer__contact-link" href="tel:<?php the_field('footer_phone1', 'option'); ?>">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/call_footer.svg" alt="phone" />
									<span><?php the_field('footer_phone1', 'option'); ?></span>
								</a>
								<a class="footer__contact-link" href="tel:<?php the_field('footer_phone2', 'option'); ?>">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/viber.svg" alt="viber" />
									<span><?php the_field('footer_phone2', 'option'); ?></span>
								</a>
							</div>
							<div class="footer__content" data-aos="show-up-20" data-aos-delay="800">
								<a class="footer__contact-link" href="mailto:<?php the_field('footer_email', 'option'); ?>">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/mail_footer.svg" alt="mail" />
									<span><?php the_field('footer_email', 'option'); ?></span>
								</a>
								<a class="footer__contact-link mb-0" href="/">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/geo.svg" alt="geo" />
									<span><?php the_field('footer_address', 'option'); ?></span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-2 d-none d-lg-block">
						<div class="footer__socials-wrap" data-aos="show-up-20" data-aos-delay="1000">
							<a class="footer__social" href="<?php the_field('instagram', 'option'); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/instagram.svg" alt="instagram" /></a>
							<a class="footer__social" href="<?php the_field('facebook', 'option'); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/facebook.svg" alt="facebook" /></a>
						</div>
					</div>
					<div class="col-12">
						<div class="copyright-wrap">
							<p class="copyright-text" data-aos="show-up-20" data-aos-delay="1200" data-aos-anchor=".footer">Aludis © 2020</p>
							<a class="copyright-text" href="/" data-aos="show-up-20" data-aos-delay="1400" data-aos-anchor=".footer">by Command+X</a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<div class="global-menu">
			<ul class="global-menu__links-list">
				<li class="global-menu__links-item">
					<a class="global-menu__link" href="/category.html">
						<span>Ворота</span>
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/link_arrow.svg" alt="link" />
					</a>
					<div class="global-menu__sub-wrap">
						<div class="global-menu__sub-list">
							<a class="global-menu__sub-link" href="/">Подкатегория 1</a>
							<a class="global-menu__sub-link" href="/">Подкатегория 2</a>
							<a class="global-menu__sub-link" href="/">Подкатегория 3</a>
						</div>
					</div>
				</li>
				<li class="global-menu__links-item">
					<a class="global-menu__link" href="/category.html">
						<span>Роллеты</span>
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/link_arrow.svg" alt="link" />
					</a>
					<div class="global-menu__sub-wrap">
						<div class="global-menu__sub-list">
							<a class="global-menu__sub-link" href="/">Подкатегория 1</a>
							<a class="global-menu__sub-link" href="/">Подкатегория 2</a>
							<a class="global-menu__sub-link" href="/">Подкатегория 3</a>
						</div>
					</div>
				</li>
				<li class="global-menu__links-item">
					<a class="global-menu__link" href="/category.html">
						<span>Автоматика</span>
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/link_arrow.svg" alt="link" />
					</a>
					<div class="global-menu__sub-wrap">
						<div class="global-menu__sub-list">
							<a class="global-menu__sub-link" href="/">Подкатегория 1</a>
							<a class="global-menu__sub-link" href="/">Подкатегория 2</a>
							<a class="global-menu__sub-link" href="/">Подкатегория 3</a>
						</div>
					</div>
				</li>
				<li class="global-menu__links-item">
					<a class="global-menu__link" href="/contacts.html"><span>Контакты</span></a>
				</li>
			</ul>
			<a class="global-menu__contact-link" href="mailto:office.hordis@gmail.com">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/mail_footer.svg" alt="mail" />
				<span>office.hordis@gmail.com</span>
			</a>
		</div>
		<div class="modal fade" id="feedback" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button class="modal-close" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/close.svg" alt="close" /></button>
					</div>
					<div class="modal-body">
						<h3 class="modal-title">Бесплатная консультация</h3>
						<p class="modal-sub-title">Оставьте свои контакты, опишите тему вопроса и наш менеджер проконсультирует вас в ближайшее время</p>
						<form class="form form-send-mail" id="feedback-modal-form" method="POST" action="<?php echo admin_url('admin-ajax.php?action=send_mail'); ?>" data-aos="show-up-20" data-aos-delay="400">
							<div class="form-group"><input class="form-control" name="name" placeholder="Имя" required /></div>
							<div class="form-group"><input class="form-control" type="tel" name="phone" placeholder="Номер телефона" required /></div>
							<div class="form-group"><input class="form-control" name="question" placeholder="Опишите вопрос" required /></div>
							<button class="custom-btn" type="submit"><span>Отправить заявку</span></button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button class="modal-close" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/close.svg" alt="close" /></button>
					</div>
					<div class="modal-body">
						<h3 class="modal-title">Спасибо, заявка принята!</h3>
						<p class="modal-sub-title">
							Наш менеджер свяжется с вами для дальнейшей консультации по интересующему вас вопросу в ближайшее время. Благодарим за доверие!
						</p>
					</div>
				</div>
			</div>
		</div>
 
		<script defer="defer" src="<?php echo get_stylesheet_directory_uri() ?>/js/main.bundle.js"></script>
		<script defer="defer" src="<?php echo get_stylesheet_directory_uri() ?>/js/393.bundle.js"></script>
		<script defer="defer" src="<?php echo get_stylesheet_directory_uri() ?>/js/745.bundle.js"></script>
        <?php wp_footer(); ?>
	</body>
</html>