<?php /* Template Name: Contact Page */ ?>
  

<?php get_header(); ?>


<main id="contacts-page">
			<section class="contacts-main-section">
				<div class="container">
					<div class="breadcrumb" data-aos="show-up-20">
						<a class="breadcrumb-item" href="/">Главная</a>
						<a class="breadcrumb-item" href="/contacts.html">Контакты</a>
					</div>
					<div class="row">
						<div class="col-12 col-lg-4">
							<h1 class="section-title" data-aos="show-up-20" data-aos-delay="200">Контакты</h1>
							<div class="contacts-wrap with-divider" data-aos="show-up-20" data-aos-delay="400">
								<a class="contacts-item" href="/">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/geo.svg" alt="geo" />
									<span><?php the_field('контакты_адрес', 'option'); ?></span>
								</a>
								<a class="contacts-item" href="mailto:<?php the_field('контакты_email', 'option'); ?>">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/mail_footer.svg" alt="mail" />
									<span><?php the_field('контакты_email', 'option'); ?></span>
								</a>
							</div>
							<div class="contacts-wrap" data-aos="show-up-20" data-aos-delay="600">
								<a class="contacts-item" href="tel:8 (965) 763 07 05">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/call.svg" alt="call" />
									<span><?php the_field('контакты_телефон_1', 'option'); ?></span>
								</a>
								<a class="contacts-item" href="tel:8 (965) 763 07 05">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/viber.svg" alt="viber" />
									<span><?php the_field('контакты_телефон_2', 'option'); ?></span>
								</a>
							</div>
							<div class="contacts-socials d-none d-lg-flex" data-aos="show-up-20" data-aos-delay="800">
								<a class="social-link" href="<?php the_field('контакты_ссылка_на_инстаграм', 'option'); ?>"><img class="svg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/contact_instagram.svg" alt="instagram" /></a>
								<a class="social-link" href="<?php the_field('контактконтакты_ссылка_на_фейсбукы_телефон_1', 'option'); ?>"><img class="svg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/contact_facebook.svg" alt="facebook" /></a>
							</div>
						</div>
						<div class="col-12 col-lg-5 offset-lg-3 feedback-section">
							<h2 class="section-title" data-aos="show-up-20" data-aos-delay="400">Связаться с нами</h2>
							<form class="form form-send-mail" id="feedback-form" method="POST" action="<?php echo admin_url('admin-ajax.php?action=send_mail'); ?>" data-aos="show-up-20" data-aos-delay="400">
								<div class="form-group"><input class="form-control" name="name" placeholder="Имя" required /></div>
								<div class="form-group"><input class="form-control" type="tel" name="phone" placeholder="Номер телефона" required /></div>
								<div class="form-group"><input class="form-control" name="question" placeholder="Опишите вопрос" required /></div>
								<button class="custom-btn" type="submit"><span>Отправить заявку</span></button>
							</form>
						</div>
					</div>
				</div>
			</section>
			<section class="contacts-map-section" data-aos="clip-right">
				<div class="map-wrap">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2747.9360246362517!2d30.739194215714416!3d46.46977517380257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c631789b3e32f1%3A0x4c251eb329fa48c3!2z0YPQu9C40YbQsCDQn9GD0YjQutC40L3RgdC60LDRjywgMTIzLCDQntC00LXRgdGB0LAsINCe0LTQtdGB0YHQutCw0Y8g0L7QsdC70LDRgdGC0YwsIDY1MDAw!5e0!3m2!1sru!2sua!4v1605603879775!5m2!1sru!2sua"
						frameborder="0"
						style="border: 0"
						allowfullscreen=""
						aria-hidden="false"
						tabindex="0"
					></iframe>
				</div>
			</section>
		</main>


<?php get_footer(); ?>