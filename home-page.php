<?php /* Template Name: Home Page */ ?>
  

<?php get_header(); ?>


<main id="home-page">
			<section class="home-main-section">
				<div class="slider-wrap main-slider">
					<div class="slider" id="main-slider">



                    <?php 
                        $temp = $wp_query; $wp_query= null;
                        $wp_query = new WP_Query(); $wp_query->query('showposts=10&post_type=slider' . '&paged='.$paged);
                        
                        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                            <div class="slide">
                                <div class="main-slider__image">
                                <?php if ( has_post_thumbnail() ) { ?> 
                                        <?php the_post_thumbnail('slider',array('title' => ''));?>
                                <?php } ?>
                                </div>
                                <div class="container">
                                    <h2 class="main-slider__title" data-aos="show-up-20"><?php the_title(); ?></h2>
									<p class="main-slider__sub-title" data-aos="show-up-20" data-aos-delay="200"><?php the_content();?></p>
									<?php if( get_field('slider_button_link') ): ?>
										<a href="<?php the_field('slider_button_link'); ?>" class="custom-btn slider-btn"><?php the_field('slider_button_text'); ?></a>
									<?php else : ?>
										<button class="custom-btn" data-toggle="modal" data-target="#feedback" data-aos="show-up-20" data-aos-delay="400">
                                        	<span><?php the_field('slider_button_text'); ?></span>
                                    	</button>
									<?php endif;  ?>
                                </div>
                            </div>

                        <?php endwhile; ?>	
					</div>
				</div>
			</section>
			<section class="home-service-section">
				<div class="scroll-wrap" data-aos="show-up-20">
					<span>скролл</span>
					<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/scroll.svg" alt="scroll" />
				</div>
				<div class="container">
					<h2 class="section-title d-block d-lg-none">Услуги</h2>
					<div class="row">
                    <?php
                    $args = array(
                        'orderby' => 'id',
                        'parent' => 0,
                        'exclude' => 15
                      );
                    $categories = get_categories($args);
                    foreach($categories as $category) {
                        $current_term = get_queried_object(); 
                        $image = get_field('category_image', $category->taxonomy . '_' . $category->term_id );
                    
                        echo '<div class="col-12 col-lg-4"><a class="home-service-card" href="' . get_category_link($category->term_id) . '" data-aos="show-up-20" data-aos-delay="200">' . '<div class="home-service-card__image"><img src="'. $image . '" alt="' . $category->name . '" /></div><h3 class="home-service-card__title">' . $category->name . '</h3>' . '</a></div>';
                    } ?>
						
					</div>
				</div>
			</section>
			<section class="home-advantage-section">
				<div class="container">
					<h2 class="section-title" data-aos="show-up-20">Преимущества</h2>
					<div class="row">
						<div class="col-12 col-lg-3">
							<div class="advantage-card">
								<div class="advantage-card__image" data-aos="show-up-20" data-aos-delay="200">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/advantage_1.svg" alt="<?php the_field('deadline', 'option'); ?>" />
								</div>
								<h3 class="advantage-card__title" data-aos="show-up-20" data-aos-delay="400"><?php the_field('deadline', 'option'); ?></h3>
								<p class="advantage-card__desc" data-aos="show-up-20" data-aos-delay="600"><?php the_field('deadline_d', 'option'); ?></p>
							</div>
						</div>
						<div class="col-12 col-lg-3">
							<div class="advantage-card">
								<div class="advantage-card__image" data-aos="show-up-20" data-aos-delay="200">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/advantage_2.svg" alt="<?php the_field('cost', 'option'); ?>" />
								</div>
								<h3 class="advantage-card__title" data-aos="show-up-20" data-aos-delay="400"><?php the_field('cost', 'option'); ?></h3>
								<p class="advantage-card__desc" data-aos="show-up-20" data-aos-delay="600"><?php the_field('cost_d', 'option'); ?></p>
							</div>
						</div>
						<div class="col-12 col-lg-3">
							<div class="advantage-card">
								<div class="advantage-card__image" data-aos="show-up-20" data-aos-delay="200">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/advantage_3.svg" alt="<?php the_field('docs', 'option'); ?>" />
								</div>
								<h3 class="advantage-card__title" data-aos="show-up-20" data-aos-delay="400"><?php the_field('docs', 'option'); ?></h3>
								<p class="advantage-card__desc" data-aos="show-up-20" data-aos-delay="600"><?php the_field('docs_d', 'option'); ?></p>
							</div>
						</div>
						<div class="col-12 col-lg-3">
							<div class="advantage-card">
								<div class="advantage-card__image" data-aos="show-up-20" data-aos-delay="200">
									<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/advantage_4.svg" alt="<?php the_field('worktime', 'option'); ?>" />
								</div>
								<h3 class="advantage-card__title" data-aos="show-up-20" data-aos-delay="400"><?php the_field('worktime', 'option'); ?></h3>
								<p class="advantage-card__desc" data-aos="show-up-20" data-aos-delay="600"><?php the_field('worktime_d', 'option'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="home-about-section">
				<div class="container">
					<div class="row flex-lg-row-reverse">
						<div class="col-12 col-lg-6 p-0">
							<div class="about-image" data-aos="clip-right" data-aos-delay="200"><img src="<?php the_field('about_img', 'option'); ?>" alt="About" /></div>
						</div>
						<div class="col-12 col-lg-6">
							<div class="about-info">
								<h2 class="section-title" data-aos="show-up-20"><?php the_field('about_title', 'option'); ?></h2>
								<p class="about-info__desc" data-aos="show-up-20" data-aos-delay="200"><?php the_field('about_content', 'option'); ?>
								</p>
								<a class="custom-btn" href="<?php the_field('about_button_link', 'option'); ?>" data-aos="show-up-20" data-aos-delay="400"><span><?php the_field('about_button_text', 'option'); ?></span></a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="home-stock-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-2"><h2 class="section-title" data-aos="show-up-20">Акции</h2></div>
						<div class="col-12 col-lg-10">
							<div class="slider-wrap stock-slider">
								<div class="slider-nav" data-aos="show-up-20" data-aos-delay="200">
									<div class="slider-arrow slider-arrow_prev"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/slider_arrow.svg" alt="Prev" /></div>
									<div class="slider-count">
										<p class="slider-count__text slider-count_current">1</p>
										<p class="slider-count__text">/</p>
										<p class="slider-count__text slider-count_total">2</p>
									</div>
									<div class="slider-arrow slider-arrow_next"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/slider_arrow.svg" alt="Next" /></div>
								</div>
								<div class="slider" id="stock-slider">
                                <?php 
                                $temp = $wp_query; $wp_query= null;
                                $wp_query = new WP_Query(); $wp_query->query('cat=15&showposts=10&post_type=post' . '&paged='.$paged);
                                
                                while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
									<div class="slide" data-aos="show-up-20" data-aos-delay="400">
										<div class="stock-slider__image">
                                            <?php if ( has_post_thumbnail() ) { ?> 
                                                <?php the_post_thumbnail('homepage',array('title' => ''));?>
                                            <?php } ?>
                                        </div>
										<h4 class="stock-slider__title"><?php the_title(); ?></h4>
										<p class="stock-slider__desc"><?php the_content();?></p>
										<?php if( get_field('price_new') ): ?>
										<div class="stock-slider__price-wrap">
											<p class="stock-slider__price stock-slider__price_new"><?php the_field('price_new'); ?> р</p>
											<p class="stock-slider__price stock-slider__price_old"><?php the_field('price_old'); ?> р</p>
										</div>
										<?php endif; ?>
										<button class="custom-btn" type="button" data-toggle="modal" data-target="#feedback"><span>Узнать детали</span></button>
									</div>
                                <?php endwhile; ?>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="home-scheme-section">
				<div class="container">
					<h2 class="section-title" data-aos="show-up-20"><?php the_field('scheme_title', 'option'); ?></h2>
					<div class="row">
						<div class="col-12 col-lg-3">
							<div class="scheme-card" data-aos="show-up-20" data-aos-delay="200">
								<div class="scheme-card__number"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/number_one.svg" alt="Подписание договора" /></div>
								<div class="scheme-card__info">
									<h3 class="scheme-card__title"><?php the_field('scheme_title1', 'option'); ?></h3>
									<p class="scheme-card__desc"><?php the_field('scheme_content1', 'option'); ?></p>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-3">
							<div class="scheme-card" data-aos="show-up-20" data-aos-delay="400">
								<div class="scheme-card__number"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/number_two.svg" alt="Анализ документов" /></div>
								<div class="scheme-card__info">
									<h3 class="scheme-card__title"><?php the_field('scheme_title2', 'option'); ?></h3>
									<p class="scheme-card__desc"><?php the_field('scheme_content1', 'option'); ?></p>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-3">
							<div class="scheme-card" data-aos="show-up-20" data-aos-delay="600">
								<div class="scheme-card__number"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/number_three.svg" alt="Обследование объекта" /></div>
								<div class="scheme-card__info">
									<h3 class="scheme-card__title"><?php the_field('scheme_title3', 'option'); ?></h3>
									<p class="scheme-card__desc"><?php the_field('scheme_content1', 'option'); ?></p>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-3">
							<div class="scheme-card" data-aos="show-up-20" data-aos-delay="800">
								<div class="scheme-card__number"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/number_four.svg" alt="Подготовка документов" /></div>
								<div class="scheme-card__info">
									<h3 class="scheme-card__title"><?php the_field('scheme_title4', 'option'); ?></h3>
									<p class="scheme-card__desc"><?php the_field('scheme_content1', 'option'); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="feedback-section">
				<div class="background-image"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/default.jpeg" alt="Background" /></div>
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-6">
							<h2 class="section-title" data-aos="show-up-20">Бесплатная консультация</h2>
							<p class="section-desc" data-aos="show-up-20" data-aos-delay="200">
								Оставьте свои контакты, опишите тему вопроса и наш менеджер проконсультирует вас в ближайшее время
							</p>
                            <form class="form form-send-mail" id="feedback-form" method="POST" action="<?php echo admin_url('admin-ajax.php?action=send_mail'); ?>" data-aos="show-up-20" data-aos-delay="400">
								<div class="form-group"><input class="form-control" name="name" placeholder="Имя" required /></div>
								<div class="form-group"><input class="form-control" type="tel" name="phone" placeholder="Номер телефона" required /></div>
								<div class="form-group"><input class="form-control" name="question" placeholder="Опишите вопрос" required /></div>
								<button class="custom-btn" type="submit"><span>Отправить заявку</span></button>
							</form>
						</div>
					</div>
				</div>
			</section>
			<section class="home-reviews-section">
				<div class="container">
					<h2 class="section-title" data-aos="show-up-20">Отзывы</h2>
					<div class="slider-wrap reviews-slider">
						<div class="slider" id="reviews-slider">
                        <?php 
                                $temp = $wp_query; $wp_query= null;
                                $wp_query = new WP_Query(); $wp_query->query('showposts=10&post_type=reviews' . '&paged='.$paged);
                                
                                while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                                <div class="slide">
								<div class="review-card" data-aos="show-up-20" data-aos-delay="400">
									<div class="review-card__info-wrap">
										<div class="review-card__person-info">
											<div class="review-card__person-image d-none d-lg-block">
                                            <?php if ( has_post_thumbnail() ) { ?> 
                                                <?php the_post_thumbnail('avatar',array('title' => ''));?>
                                            <?php } ?>
											</div>
											<p class="review-card__person-name"><?php the_title(); ?></p>
										</div>
										<p class="review-card__date d-block d-lg-none"><?php the_time('d.m.Y'); ?></p>
									</div>
									<div class="review-card__desc"><?php the_content();?></div>
									<p class="review-card__date d-none d-lg-block"><?php the_time('d.m.Y'); ?></p>
									<div class="review-card__see-more">развернуть</div>
								</div>
							    </div>
                            <?php endwhile; ?>	


						</div>
						<div class="slider-nav" data-aos="show-up-20" data-aos-delay="200">
							<div class="slider-arrow slider-arrow_prev"><span>назад</span></div>
							<div class="slider-count">
								<p class="slider-count__text slider-count_current">1</p>
								<p class="slider-count__text">/</p>
								<p class="slider-count__text slider-count_total">2</p>
							</div>
							<div class="slider-arrow slider-arrow_next"><span>далее</span></div>
						</div>
						<button class="custom-btn leave-review" type="button" data-toggle="modal" data-target="#review"><span>Оставить отзыв</span></button>
					</div>
				</div>
			</section>
		</main>



<?php get_footer(); ?>