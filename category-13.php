<?php get_header(); ?>
<?php
    $this_category = get_category($cat);
	//If category is parent, list it
    if ($this_category->category_parent == 0) {
		$this_category->category_parent = $cat;
    } else { // If category is not parent, list parent category
		$parent_category = get_category($this_category->category_parent);
		//get the parent category id
		$ParentCatId = $parent_category->cat_ID;
		}
		$childcategories = array();
		$catx = $ParentCatId;
		$cats = get_categories('parent='.$catx);
		foreach($cats as $cat) {
			$childcategories[] = $cat->term_id; } ;
		//if it is a child category, add a class with the parent category id
		if( is_category( $childcategories ) ) {
			$class = 'parent-category-'.$ParentCatId;
		}
?> 
<main id="sub-category-page">
			<section class="category-main-section">
				<div class="section-bg"><img src="<?php $category = get_queried_object(); $image = get_field('category_image', $category); echo $image; ?>" alt="background" /></div>
				<div class="container">
					<div class="breadcrumb" data-aos="show-up-20">
						<a class="breadcrumb-item" href="/">Главная</a>
						<a class="breadcrumb-item" href="
						<?php 	$thiscat =  get_query_var('cat'); // The id of the current category
								$catobject = get_category($thiscat,false); // Get the Category object by the id of current category
								$parentcat = $catobject->category_parent; // the id of the parent category 
								echo get_category_link( $parentcat ); ?>
						
						"><?php echo get_cat_name($parentcat);?></a>
						<a class="breadcrumb-item" href="<?php echo get_category_link( $category_id ); ?>"><?php single_term_title(); ?></a>
					</div>
					<div class="row">
						<div class="col-12 col-lg-6">
							<h1 class="section-title" data-aos="show-up-20" data-aos-delay="200"><?php single_term_title(); ?></h1>
							<p class="section-sub-title" data-aos="show-up-20" data-aos-delay="400"><?php if ( '' != the_archive_description() ) { echo esc_html( the_archive_description() ); } ?></p>
							<button
								class="custom-btn secondary"
								type="button"
								data-toggle="modal"
								data-target="#feedback"
								data-aos="show-up-20"
								data-aos-delay="600"
							>
								<span>Получить консультацию</span>
							</button>
						</div>
						<div class="col-12 offset-lg-2 col-lg-4">
							<div class="sub-categories" data-aos="show-up-20" data-aos-delay="400">


							<?php if (get_field('дов-ссылка1', 'option')) { ?>
								<a class="sub-category-link" href="<?php the_field('дов-ссылка1', 'option'); ?>"><?php the_field('дов-ссылкатекст1', 'option'); ?></a>
							<?php } ?>
							<?php if (get_field('дов-ссылка2', 'option')) { ?>
								<a class="sub-category-link" href="<?php the_field('дов-ссылка2', 'option'); ?>"><?php the_field('дов-ссылкатекст2', 'option'); ?></a>
							<?php } ?>
							<?php if (get_field('дов-ссылка3', 'option')) { ?>
								<a class="sub-category-link" href="<?php the_field('дов-ссылка3', 'option'); ?>"><?php the_field('дов-ссылкатекст3', 'option'); ?></a>
							<?php } ?>


							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="home-stock-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-2"><h2 class="section-title" data-aos="show-up-20">Акции</h2></div>
						<div class="col-12 col-lg-10">
							<div class="slider-wrap stock-slider">
								<div class="slider-nav" data-aos="show-up-20" data-aos-delay="200">
									<div class="slider-arrow slider-arrow_prev"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/slider_arrow.svg" alt="Prev" /></div>
									<div class="slider-count">
										<p class="slider-count__text slider-count_current">1</p>
										<p class="slider-count__text">/</p>
										<p class="slider-count__text slider-count_total">2</p>
									</div>
									<div class="slider-arrow slider-arrow_next"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/slider_arrow.svg" alt="Next" /></div>
								</div>
								<div class="slider" id="stock-slider">
									<?php $args1 = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'Акции,Для откатных ворот');
									$myposts1 = get_posts( $args1 );
									foreach ( $myposts1 as $post ) : setup_postdata( $post ); ?>
									<div class="slide" data-aos="show-up-20" data-aos-delay="400">
										<div class="stock-slider__image">
                                            <?php if ( has_post_thumbnail() ) { ?> 
                                                <?php the_post_thumbnail('homepage',array('title' => ''));?>
                                            <?php } ?>
                                        </div>
										<h4 class="stock-slider__title"><?php the_title(); ?></h4>
										<p class="stock-slider__desc"><?php the_content();?></p>
										<?php if( get_field('price_new') ): ?>
										<div class="stock-slider__price-wrap">
											<p class="stock-slider__price stock-slider__price_new"><?php the_field('price_new'); ?> р</p>
											<p class="stock-slider__price stock-slider__price_old"><?php the_field('price_old'); ?> р</p>
										</div>
										<?php endif; ?>
										<button class="custom-btn" type="button" data-toggle="modal" data-target="#feedback"><span>Узнать детали</span></button>
									</div>
									<?php endforeach; 
            wp_reset_postdata();?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="consultation-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-7">
							<div class="consultation-section__info" data-aos="clip-right">
							<h2 class="consultation-section__title" data-aos="show-up-20" data-aos-delay="400"><?php the_field('consultation_title', 'option'); ?></h2>
								<p class="consultation-section__desc" data-aos="show-up-20" data-aos-delay="600"><?php the_field('consultation_descr', 'option'); ?></p>
								<div data-aos="show-up-20" data-aos-delay="600">
									<a class="consultation-section__phone" href="tel:<?php the_field('consultation_phone', 'option'); ?>"><?php the_field('consultation_phone', 'option'); ?></a>
								</div>
								<button
									class="custom-btn inverse"
									type="button"
									data-toggle="modal"
									data-target="#feedback"
									data-aos="show-up-20"
									data-aos-delay="800"
								>
									<span><?php the_field('consultation_button', 'option'); ?></span>
								</button>
							</div>
						</div>
						<div class="col-12 col-lg-5">
							<div class="consultation-section__image" data-aos="clip-right" data-aos-delay="400">
								<img src="<?php the_field('изображение_консультации', 'option'); ?>" alt="consultation" />
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php if( have_rows('дов-блок_преимуществ', 'option') ): ?>
												<?php while( have_rows('дов-блок_преимуществ', 'option') ): the_row(); ?>
												

			<section class="sub-category-section" id="<?php the_sub_field('dov-yakor_bloka'); ?>">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-6 p-0">
							<div class="sub-category__image" data-aos="clip-right"><img src="<?php the_sub_field('дов-изображение_блока'); ?>" alt="subcategory" /></div>
						</div>
						<div class="col-12 col-lg-6">
							<div class="sub-category__info">
								<h3 class="sub-category__title" data-aos="show-up-20" data-aos-delay="200"><?php the_sub_field('дов-заголовок_блока'); ?></h3>
								<p class="sub-category__desc" data-aos="show-up-20" data-aos-delay="400"><?php the_sub_field('дов-описание_блока'); ?></p>
								<a class="custom-btn secondary" href="<?php the_sub_field('дов-ссылка_кнопки'); ?>" data-aos="show-up-20" data-aos-delay="600"><span><?php the_sub_field('дов-надпись_на_кнопке'); ?></span></a>
							</div>
						</div>
						<div class="col-12 col-advantages">
							<div class="row">
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="600">
										<div class="advantage-card__icon"><?php the_sub_field('дов-иконка_1'); ?></div>
										<h4 class="advantage-card__title"><?php the_sub_field('дов-заголовок_преимущества_1'); ?></h4>
										<p class="advantage-card__desc"><?php the_sub_field('дов-описание_преимущества_1'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="600">
										<div class="advantage-card__icon"><?php the_sub_field('дов-иконка_2'); ?></div>
										<h4 class="advantage-card__title"><?php the_sub_field('дов-заголовок_преимущества_2'); ?></h4>
										<p class="advantage-card__desc"><?php the_sub_field('дов-описание_преимущества_2'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="600">
										<div class="advantage-card__icon"><?php the_sub_field('дов-иконка_3'); ?></div>
										<h4 class="advantage-card__title"><?php the_sub_field('дов-заголовок_преимущества_3'); ?></h4>
										<p class="advantage-card__desc"><?php the_sub_field('дов-описание_преимущества_3'); ?></p>
									</div>
								</div>
								<div class="col-12 col-lg-3">
									<div class="advantage-card" data-aos="show-up-20" data-aos-delay="600">
										<div class="advantage-card__icon"><?php the_sub_field('дов-иконка_4'); ?></div>
										<h4 class="advantage-card__title"><?php the_sub_field('дов-заголовок_преимущества_4'); ?></h4>
										<p class="advantage-card__desc"><?php the_sub_field('дов-описание_преимущества_4'); ?></p>
									</div>
								</div>

							</div>
						</div>
						<div class="col-12 col-examples">
							<div class="row">
								<div class="col-12">
									<div class="row">
										<div class="col-12 col-lg-7"><h3 class="section-title" data-aos="show-up-20"><?php the_field('дов-гр_заголовок_блока', 'option'); ?></h3></div>
										<div class="col-12 col-lg-4 offset-lg-1">
											<p class="section-sub-title" data-aos="show-up-20" data-aos-delay="200"><?php the_field('дов-гр_описание_блока', 'option'); ?></p>
										</div>
									</div>
								</div>
								<div class="col-12">
									<div class="slider-wrap">
										<div class="example-slider slider" data-aos="show-up-20" data-aos-delay="400">

										<?php if( have_rows('дов-гр_карточка_товара', 'option') ): ?>
												<?php while( have_rows('дов-гр_карточка_товара', 'option') ): the_row(); ?>
											<div class="slide example-slide">
												<div class="example-inner-slider slider">
												<?php if( have_rows('дов-гр_изображения_карточки_товара', 'option') ): ?>
												<?php while( have_rows('дов-гр_изображения_карточки_товара', 'option') ): the_row(); ?>
													<div class="slide example-inner-slide">
														<div class="example-card__image"><img src="<?php the_sub_field('дов-гр_изображение_товара'); ?>" alt="example" /></div>
													</div>
													<?php endwhile; ?>
												<?php endif; ?>
												</div>
												<div class="example-card">
													<p class="example-card__title"><?php the_sub_field('дов-гр_заголовок_карточки_товара'); ?></p>
													<p class="example-card__desc"><?php the_sub_field('дов-гр_описание_карточки_товара'); ?></p>
													<p class="example-card__parameters"><?php the_sub_field('дов-гр_характеристики_карточки_товара'); ?></p>
													<button class="custom-btn secondary" type="button" data-toggle="modal" data-target="#feedback">
														<span>Узнать стоимость</span>
													</button>
												</div>
											</div>
												<?php endwhile; ?>
												<?php endif; ?>
										</div>
										<div class="slider-nav" data-aos="show-up-20" data-aos-delay="600">
											<div class="slider-arrow slider-arrow_prev"><span>назад</span></div>
											<div class="slider-count">
												<p class="slider-count__text slider-count_current">1</p>
												<p class="slider-count__text">/</p>
												<p class="slider-count__text slider-count_total">2</p>
											</div>
											<div class="slider-arrow slider-arrow_next"><span>далее</span></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>												
			
			<?php endwhile; ?>
			<?php endif; ?>


			<section class="sub-category-video-section">
				<div class="container">
					<h2 class="section-title" data-aos="show-up-20"><?php the_field('дов-заголовок_видео', 'option'); ?></h2>
					<div class="video-wrap" data-aos="show-up-20" data-aos-delay="200"><?php the_field('дов-код_видео', 'option'); ?></div>
				</div>
			</section>
			<section class="consultation-section">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-7">
							<div class="consultation-section__info" data-aos="clip-right">
							<h2 class="consultation-section__title" data-aos="show-up-20" data-aos-delay="400"><?php the_field('consultation_title', 'option'); ?></h2>
								<p class="consultation-section__desc" data-aos="show-up-20" data-aos-delay="600"><?php the_field('consultation_descr', 'option'); ?></p>
								<div data-aos="show-up-20" data-aos-delay="600">
									<a class="consultation-section__phone" href="tel:<?php the_field('consultation_phone', 'option'); ?>"><?php the_field('consultation_phone', 'option'); ?></a>
								</div>
								<button
									class="custom-btn inverse"
									type="button"
									data-toggle="modal"
									data-target="#feedback"
									data-aos="show-up-20"
									data-aos-delay="800"
								>
									<span><?php the_field('consultation_button', 'option'); ?></span>
								</button>
							</div>
						</div>
						<div class="col-12 col-lg-5">
							<div class="consultation-section__image" data-aos="clip-right" data-aos-delay="400">
								<img src="<?php the_field('изображение_консультации', 'option'); ?>" alt="consultation" />
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>

<?php get_footer(); ?>